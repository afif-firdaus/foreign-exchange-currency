import React from 'react';
import './App.scss';
import Currency from './components/Currency/Currency';

const App = () => {
  return (
    <div className="main">
      <div className="card-container">
        <Currency />
      </div>
    </div>
  )
}

export default App;