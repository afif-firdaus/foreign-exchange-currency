import React from 'react';
import { Modal, ModalBody } from 'reactstrap';

const Modals = (props) => {
  return (
    <Modal isOpen={props.modal} toggle={props.toggle}>
      <ModalBody className="h-50 text-info">
          <strong>{props.value}</strong> has been added!
      </ModalBody>
    </Modal>
  )
}

export default Modals;
