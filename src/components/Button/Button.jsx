import React from 'react';
import Rates from '../Rates/Rates';
import add from '../../assets/add.svg';
import './Button.scss';

const Button = (props) => {
  return (
    <div className="button">
      {
        !props.isOpen ? 
        <div className="add-button" onClick={props.toggle}>
          <img src={add} alt="" width={30}/>
          <p>Add more currencies</p>  
        </div> :
        <div className="submit-button">
          <Rates rates={props.rates} change={props.changeValue}/>
          <button type="submit" onClick={props.addNewCurrency}>Submit</button>
        </div>
      }
    </div>
  )
}

export default Button;
