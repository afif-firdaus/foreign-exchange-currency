import React from 'react';
import './NoCurrency.scss';

const NoCurrency = () => {
  return (
    <div className="no-currency">
      <p>Click add more currencies to start converting</p>
    </div>
  )
}

export default NoCurrency;