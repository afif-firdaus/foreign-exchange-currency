import React, { Fragment, useEffect, useState } from 'react';
import NoCurrency from '../No Currency/NoCurrency';
import InputValue from '../Input Value/InputValue';
import Button from '../Button/Button';
import Modals from '../Modals/Modals';
import axios from 'axios';
import './Currency.scss';
import remove from '../../assets/remove.svg';

const Currency = () => {
  const [modal, setModal] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [currency, setCurrency] = useState([]);
  const [updateCurrency, setUpdateCurrency] = useState()
  const [value, setValue] = useState();
  const [rates, setRates] = useState([]);
  const [rateValue, setRateValue] = useState(1);

  useEffect(() => {
    axios.get('/latest?base=USD')
      .then(res => {
        setRates(res.data.rates)
        setUpdateCurrency(res.data.date)
    })
  }, []);

  const toggleModal = () => setModal(!modal);

  const toggleHander = () => setIsOpen(!isOpen);

  const changeValue = e => {
    setValue(e.target.value);
  }

  const changeRateValue = (e) => {
    setRateValue(e.target.value)
  }

  const addNewCurrency = () => {
    if (currency.includes(value)) {
      setModal(!modal)
    }
    setCurrency([...currency, value]);
    setIsOpen(!isOpen);
  }

  const removeCurrency = (e) => {
    let newCurrency = [...currency];
    let index = newCurrency.indexOf(e.currentTarget.getAttribute('value'))
    if (index !== -1) {
      newCurrency.splice(index, 1);
      setCurrency(newCurrency);
    }
  } 

  const filteredCurrency = Object.keys(rates)
  .filter(key => currency.includes(key))
  .reduce((obj, key) => {
    obj[key] = rates[key];
    return obj;
  }, {});
  
  return (
    <Fragment>
      <InputValue value={rateValue} changeRate={changeRateValue}/>
      <div className="currency-wrapper">
        {
          currency.length > 0 ? Object.keys(filteredCurrency).map((data, index) => {
            return (
              <div className="currency-card" key={index}>
                <div className="currency-info">
                  <div className="currency-exchange">
                    <h3>{data}</h3>
                    <h3>{`${filteredCurrency[data] * rateValue}`}</h3>
                  </div>
                  <p>{data}</p>
                  <p>1 USD = {filteredCurrency[data]}</p>
                </div>
                <div className="remove-currency" value={data} onClick={removeCurrency}>
                  <img src={remove} alt="" width={25}/>
                </div>
              </div>
            )
          }) : <NoCurrency />
        }
        <p className="udate-currency"><em>Last update</em>: {updateCurrency}</p>
      </div>
      <Modals value={value} modal={modal} toggle={toggleModal}/>
      <Button 
        addNewCurrency={addNewCurrency}
        changeValue={changeValue}
        rates={rates}
        toggle={toggleHander}
        isOpen={isOpen}
      />
    </Fragment>
  )
}

export default Currency;