import React from 'react';
import './InputValue.scss';

const InputValue = (props) => {  
  return (
    <div className="containers">
      <p>USD - United States Dollars</p>
      <div className="input-number">
        <label htmlFor="number">USD</label>
        <input id="number" type="number" value={props.value} onChange={props.changeRate}/>
      </div>
    </div>
  )
}

export default InputValue;