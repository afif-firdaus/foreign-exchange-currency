import React from 'react';
import './Rates.scss';

const Rates = (props) => {
  return (
    <div className="select-currency">
      <select name="" id="select" onChange={props.change}>
        {
          Object.keys(props.rates).map(rate => {
            return <option value={rate} key={rate} >{rate}</option>
          })
        }
      </select>
    </div>
  )
}

export default Rates;